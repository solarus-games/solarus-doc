/**
\page lua_api_controls Controls

\tableofcontents

This module provides a datatype controls that represent key or joypad buttons / axis bindings.
Controls can be seen as an abstraction of the \ref lua_api_input and the \ref lua_api_joypad.
Controls are like a virtual game device that provides buttons and axes abstracted away from the actual input devices that the user is interacting with.
Of course, these virtual commands and axes are mapped to real, low-level inputs from the keyboard and/or the joypad. You script can control which keyboard and joypad inputs are associated to each controls command or axes.

\section lua_api_controls_overview Overview

\subsection lua_api_controls_overview_commands Commands

Commands are the "buttons" of solarus' virtual controls.

- "action": Contextual action such as talking, swimming, throwing, etc.
- "attack": Main attack (using the sword).
- "pause": Pausing or unpausing the game.
- "item_1": Using the equipment item placed in slot 1 (see [game:get_item_assigned()])
- "item_2": Using the equipment item placed in slot 2 (see [game:get_item_assigned()])
- "right": Moving to the right.
- "left": Moving to the left.
- "up": Moving to the top.
- "down": Moving to the bottom.

When a game command is pressed, no matter if the command came from the keyboard or the joypad, the engine performs some built-in behavior by default (like pausing the game or moving the hero to the right).
But you can also extend or override this behavior, because the engine notifies you first (see [game:on_command_pressed()]). Therefore, you usually don't have to worry about the underlying keyboard or joypad input (but if you want to you can as the game object is an \ref lua_api_input_handler_events).

\subsection lua_api_controls_overview_axes Axes

Axes are used by the engine only for movements of the hero.

- "X": Horizontal axis
- "Y": Vertical axis

\subsection lua_api_controls_overview_extending Extending

Every method or function accepting commands or axes names actually accept any string and will happily register / propagate the custom name you provided.
This permits you extend over the default commands and axes that the engine uses.
For example, you could bind additional axes of the \ref lua_api_joypad instances to custom functionalities while keeping the controls abstraction which would allow users with a keyboard to also profit from your game.

\section lua_api_controls_functions Functions of sol.controls

\subsection lua_api_controls_create_from_keyboard sol.controls.create_from_keyboard()

Creates the controls using the default mapping for the keyboard.
Default mappings are:

- Arrow keys : directions
- space : Action
- C : Attack
- X : Item1
- V : Item2
- D : Pause
- Axes are mapped to their corresponding directions.

\subsection lua_api_controls_create_from_joypad sol.controls.create_from_joypad(joypad)

Creates the controls using the default mapping for a joypad instance. Those contols respond only to the given joypad.

- \c joypad (joypad): a joypad instance to create the controls for.

Default mappings are:

- Left Stick : directions
- B : Action
- A : Attack
- X : Item1
- Y : Item2
- Start : Pause
- Left stick X : X
- Left stick Y : Y

\section lua_api_controls_methods Methods of the type controls

\subsection lua_api_controls_set_analog controls:set_analog(analog)

Enable the use of axes for built-in movements of the hero when controlled by those controls.

- \c analog (boolean): enable the analog behavior of those controls

\subsection lua_api_controls_are_analog controls:are_analog(analog)

Tells whether those controls are analog

- Return value (boolean): \c true if those controls are analog

\subsection lua_api_controls_is_pressed(command)

Tells if a command is pressed.

- \c command (string): Any valid command name

\subsection lua_api_controls_get_axis_state controls:get_axis_state(axis)

Get the last known state of an axis, returns 0 if the axis hasn't emitted since these controls where created.

- \c axis (string): an axis name, can be one of the built in names or any name
- Return value (number): axis state [-1 -> 1]

\subsection lua_api_controls_get_direction controls:get_direction()

Gets the direction8 of the commands

- Return value (number): direction8

\subsection lua_api_controls_set_keyboard_binding controls:set_keyboard_binding(command, key)

Sets the key that will trigger the given command.

- \c command (string): Any valid command name
- \c key (string): A valid key code (see \ref lua_api_input)

\subsection lua_api_controls_get_keyboard_binding controls:get_keyboard_binding(command)

Gets the key that is bound to the given command

- \c command (string): Any valid command name
- Return value (string): A key code (see \ref lua_api_input)

\subsection lua_api_controls_set_joypad_binding controls:set_joypad_binding(command, button)

- \c command (string): Any valid command name
- \c button (string): A valid joypad binding code

Binding codes must be one of the following : 
- A valid joypad button string (see \ref lua_joypad_api).
- An axis binding formed like this : <tt>"<axis_name> {+/-}"</tt> where axis_name is a valid joypad axis string (see \ref lua_joypad_api)

\subsection lua_api_controls_get_joypad_binding controls:get_joypad_binding(command)

Gets the joypad binding code bound to the given command

- \c command (string): Any valid command name
- Return value (string): A joypad binding code

\subsection lua_api_controls_set_keyboard_axis_binding controls:set_keyboard_axis_binding(axis, negative_key, positive_key)

Sets the keyboard keys that will move the given axis

- \c axis (string): Any valid axis name
- \c negative_key (string): A valid keyboard key code, triggers -1 state
- \c positive_key (string): A valid keyboard key code, triggers 1 state

\subsection lua_api_controls_get_keyboard_axis_binding controls:get_keyboard_axis_binding(axis)

Returns the keys bound to move the given axis

- Return values (string, string): The positive and negative key codes

\subsection lua_api_controls_set_joypad_axis_binding controls:set_joypad_axis_binding(axis, joypad_axis_binding)

Sets the joypad axis that will drive the given virtual axis

- \c axis (string): Any valid control axis name
- \c joypad_axis (string): A valid joypad axis binding (see \ref lua_api_controls_set_keyboard_axis_binding)

\subsection lua_api_controls_get_joypad_axis_binding controls:get_joypad_axis_binding(axis)

Gets the joypad axis that is bound to the given axis

- Return value (string) : Joypad axis binding

\subsection lua_api_controls_set_keyboard_bindings controls:set_keyboard_bindings(bindings)

Sets the commands bindings for the keyboard

- \c bindings ({string : {string}}): A table of key codes -> array of commands

\subsection lua_api_controls_get_keyboard_bindings controls:get_keyboard_bindings()

Gets the command bindings for the keyboard

- Return value ({string : {string}}): A table of key codes -> array of commands (see \ref lua_api_input)

\subsection lua_api_controls_set_joypad_bindings controls:set_joypad_bindings(bindings)

Sets the command bindings for the joypad

- \c commands ({string : {string}}): A table of joypad command bindings -> array of commands

\subsection lua_api_controls_get_joypad_bindings controls:get_joypad_bindings()

Gets the joypad command bindings

- Return value ({string : {string}}): A table of joypad command bindings -> array of commands

\subsection lua_api_controls_set_keyboard_axis_bindings controls:set_keyboard_axis_bindings(bindings)

Sets keyboard's axes bindings

- \c bindings ({string : {string}}): A table of key codes -> array of axes bindings

\subsection lua_api_controls_get_keyboard_axis_bindings controls:get_keyboard_axis_bindings()

Gets the keyboard's axes bindings

- Return values ({string : {string}}): A table of key codes -> array of axes bindings

\subsection lua_api_controls_set_joypad_axis_bindings controls:set_joypad_axis_bindings(bindings)

Sets the joypad axes bindings

- \c bindings ({string : {string}}): A table of joypad axis names -> array of axes bindings

\subsection lua_api_controls_get_joypad_axis_bindings controls:get_joypad_axis_bindings()

Gets the joypad axes bindings

- Return value ({string : {string}}): A table of joypad axis names -> array of axes bindings

\subsection lua_api_controls_get_effect controls:get_effect(command)

Gets the corresponding effect to the given command

\subsection lua_api_controls_simulate_pressed controls:simulate_pressed(command)

Simulate a command pressed event. You can also simulate custom command names.

- \c command (string): Any valid command name

\subsection lua_api_controls_simulate_released controls:simulate_released(command)

Simulate a command released event. You can also simulate custom command names.

- \c command (string): Any valid command name

\subsection lua_api_controls_simulate_axis_moved controls:simulate_axis_moved(axis, state)

Simulate an axis moved event. You can also simulate custom axis names.

- \c axis (string): Any valid axis name
- \c state (number): State to signal

\subsection lua_api_controls_set_joypad controls:set_joypad(joypad)

Sets the joypad that is bound to those controls.

- \c joypad (\ref lua_api_joypad): joypad to set, nil to disable joypad input.

\subsection lua_api_controls_get_joypad controls:get_joypad()

Gets the joypad bound to those controls. 

- Return value (\ref lua_api_joypad): bound joypad or nil if none

\subsection lua_api_controls_remove controls:remove()

Remove those controls from the event handler, effectively disabling them forever.
The lua object is still usable but will never trigger any output. If the reference is forgotten
by lua the object is freed.

\section lua_api_controls_control_handlers Control Handlers

Controls raise events that are propagated to all event handlers defined in your code.
Objects that can handle controls event are \ref lua_api_game, \ref lua_api_menu, \ref lua_api_map and \ref lua_api_state.

\subsection lua_api_controls_on_command_pressed handler:on_command_pressed(command, controls)

Called when a command is pressed on a given controls instance

- \c command (string): Any valid command name
- \c controls (controls): The controls instance that raised this event

\subsection lua_api_controls_on_command_released handler:on_command_released(command, controls)

Called when a command is released on a given controls instance

- \c command (string): Any valid command name
- \c controls (controls): The controls instance that raised this event

\subsection lua_api_controls_on_axis_moved handler:on_axis_moved(axis, state, controls)

handler:on_axis_moved(axis, state, controls)
Called when an axis is moved on a given controls instance

- \c axis (string): Any valid command name
- \c state (number): New axis state
- \c controls (controls): The controls instance that raised this event

*/
